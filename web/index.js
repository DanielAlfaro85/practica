const error = (e) => console.log(e.target.responseText);
let edit = false;
/**
 * Funcion para agregar datos
 */
function register() {
  let name = document.getElementById("name").value;
  let monto = document.getElementById("Monto").value;
  let id = document.getElementById("id").value;
  if (edit === true) {
    update(id, name, monto);
  } else {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("POST", "http://localhost:3000/api/users");
    ajaxRequest.setRequestHeader("Content-Type", "application/json");

    if (name === "" || monto === "") {
      alert("Por favor ingrese los datos solicitados");
    } else {
      const data = {
        name: document.getElementById("name").value,
        monto: document.getElementById("Monto").value,
      };
      const enviar = ajaxRequest.send(JSON.stringify(data));
    }
    show();
  }
  document.getElementById("name").value = "";
  document.getElementById("Monto").value = "";
}

/**
 * Funcion para mostrar los datos
 */
function show() {
  const url = "http://localhost:3000/api/users";
  let table = document.getElementById("tbody");
  table.innerHTML = "";
  const ajaxRequest = new XMLHttpRequest();
  ajaxRequest.addEventListener("load", (response) => {
    const lists = JSON.parse(response.target.responseText);
    lists.forEach((list) => {
      table.innerHTML += `
                <tr>
                    <td>${list._id}</td>
                    <td>${list.name}</td>
                    <td>${list.monto}</td>
                    <td><button onclick="editData('${list._id}')"> Editar </button></td>
                    <td><button onclick="deleteData('${list._id}')">Eliminar</button></td>
                </tr>
            `;
    });
  });
  ajaxRequest.addEventListener("error", error);
  ajaxRequest.open("GET", url);
  ajaxRequest.setRequestHeader("Content-Type", "application/json");
  ajaxRequest.send();
}
//Funcion para eliminar datos
function deleteData(id) {
  const ajaxRequest = new XMLHttpRequest();
  ajaxRequest.addEventListener("error", error);
  ajaxRequest.open("DELETE", `http://localhost:3000/api/users/?id=${id}`);
  ajaxRequest.setRequestHeader("Content-Type", "application/json");
  ajaxRequest.send();
  show();
}

function editData(id) {
  edit = true;
  const url = `http://localhost:3000/api/users/?id=${id}`;
  const ajaxRequest = new XMLHttpRequest();
  ajaxRequest.addEventListener("load", (response) => {
    const lists = JSON.parse(response.target.responseText);
    document.getElementById("id").value = lists._id;
    document.getElementById("name").value = lists.name;
    document.getElementById("Monto").value = lists.monto;
  });
  ajaxRequest.addEventListener("error", error);
  ajaxRequest.open("GET", url);
  ajaxRequest.setRequestHeader("Content-Type", "application/json");
  ajaxRequest.send();
}

function update(id, name, monto) {
  const ajaxRequest = new XMLHttpRequest();
  ajaxRequest.addEventListener("error", error);
  ajaxRequest.open("PATCH", `http://localhost:3000/api/users/?id=${id}`);
  ajaxRequest.setRequestHeader("Content-Type", "application/json");
  ajaxRequest.send(JSON.stringify({ name, monto }));
  edit = false;
  show();
}

show();
